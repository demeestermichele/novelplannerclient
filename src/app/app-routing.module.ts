import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard/dashboard.component';
import {ChapterComponent} from './components/chapter/chapter.component';
import {CharacterComponent} from './character/character.component';
import {PlotComponent} from './components/plot/plot.component';
import {ChapterListComponent} from './components/chapter/chapter-list/chapter-list.component';
import {CharacterDetailComponent} from './character/character-detail/character-detail.component';
import {PlotDetailComponent} from './components/plot/plot-detail/plot-detail.component';
import {PlotListComponent} from './components/plot/plot-list/plot-list.component';
import {CharacterListComponent} from './character/character-list/character-list.component';
import {ChapterDetailComponent} from './components/chapter/chapter-detail/chapter-detail.component';


const routes: Routes = [
  {path: '', component: DashboardComponent },
  {path: 'chapters', component: ChapterComponent},
  {path: 'chapters/:id', component: ChapterDetailComponent},
  {path: 'chapter-list', component: ChapterListComponent},

  {path: 'characters', component: CharacterComponent},
  {path: 'character-list', component: CharacterListComponent},
  {path: 'characters/:id', component: CharacterDetailComponent},

  {path: 'plots', component: PlotComponent},
  {path: 'plot-list', component: PlotListComponent},
  {path: 'plots/:id', component: PlotDetailComponent}
  ];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
