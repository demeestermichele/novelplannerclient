import {ButtonAComponent} from '@shared/components/button-a/button-a.component';
import {ButtonBComponent} from '@shared/components/button-b/button-b.component';

export const components: any[] = [ButtonAComponent, ButtonBComponent];
export * from '@shared/components/button-a/button-a.component';
export * from '@shared/components/button-b/button-b.component';
