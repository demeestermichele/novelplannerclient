import {Character} from '@shared/models/classes/character.model';
import {Plot} from '@shared/models/classes/plot.model';

export interface Chapter {
  id: number;
  name: string;
  chapNumber: number;
  description?: string;
  characterList?: Array<Character>;
  plotList?: Array<Plot>;
}
