import {Sex} from '@shared/models/enums/sex';
import {Chapter} from '@shared/models/classes/chapter.model';
import {Plot} from '@shared/models/classes/plot.model';

export interface Character {
  id: number;
  firstName: string;
  lastName?: string;
  sex: Sex;
  age?: number;
  birthday?: Date;
  alive?: boolean;
  death?: Date;
  // race?: Race;
  mother?: Character;
  father?: Character;
  children?: Array<Character>;
  description?: string;
  image?: string;
  chapterList?: Array<Chapter>;
  plotList?: Array<Plot>;
}
