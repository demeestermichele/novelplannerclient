import {Character} from '@shared/models/classes/character.model';
import {Chapter} from '@shared/models/classes/chapter.model';

export interface Plot {
  id: number;
  name: string;
  description: string;
  characterList?: Array<Character>;
  chapterList?: Array<Chapter>;
}
