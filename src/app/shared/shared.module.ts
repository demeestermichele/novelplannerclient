import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ButtonAComponent } from '@shared/components';
import { ButtonBComponent } from '@shared/components';
import * as fromComponents from './components';
import {NgbdSortableHeaderDirective} from '@shared/table/sortable.directive';


@NgModule({
  declarations: [
     ...fromComponents.components,
    NgbdSortableHeaderDirective
  ],
  imports: [
    CommonModule,
    FormsModule,

  ],
  exports: [
    ButtonAComponent,
    ButtonBComponent,
    FormsModule,
    ...fromComponents.components,
    NgbdSortableHeaderDirective
  ]
})
export class SharedModule {}
