import { Component, OnInit } from '@angular/core';
import {ChapterService} from '@core/services/chapter/chapter.service';
import {Location} from '@angular/common';
import {CharacterService} from '@core/services/character/character.service';
import {PlotService} from '@core/services/plot/plot.service';
import {Subscription} from 'rxjs';
import {Character} from '@shared/models/classes/character.model';
import {Chapter} from '@shared/models/classes/chapter.model';
import {Plot} from '@shared/models/classes/plot.model';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.css']
})
export class CharacterListComponent implements OnInit {
  subscription: Subscription[] = [];
  characters: Character[] = [];
  detail: Character;
  chapter: Chapter;
  chapters: Chapter[] = [];
  plot: Plot;
  plots: Plot[] = [];
  selectedCharacter: Character;

  constructor(private characterService: CharacterService,
              private chapterService: ChapterService,
              private plotService: PlotService,
              private backlocation: Location) {
  }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    console.log('this is from getAll');
    // return this.chapterService.getAllChapters();
    return this.subscription.push(this.characterService.getAllCharacters().subscribe(character => this.characters = character));
  }

  // getCharacterChapters() { //TODO needs to be written in service
  //   console.log('get characters by chapter');
  //   return this.subscription.push(this.characterService.getChapterByCharacter(this.id).subscribe(chapters => this.chapters = chapters));
  // }

  onSelect(character: Character): void {
    this.selectedCharacter = character;
  }

  goBack(): void {
    this.backlocation.back();
  }
}
