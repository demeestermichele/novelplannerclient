import {Component, Input, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {Character} from '@shared/models/classes/character.model';
import {CharacterService} from '@core/services/character/character.service';
import {Location} from '@angular/common';
import {Chapter} from '@shared/models/classes/chapter.model';
import {Plot} from '@shared/models/classes/plot.model';
import {ChapterService} from '@core/services/chapter/chapter.service';
import {PlotService} from '@core/services/plot/plot.service';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent implements OnInit {
  @Input()   character: Character;
  subscription: Subscription[] = [];
  characters: Character[] = [];
  detail: Character;
  chapter: Chapter;
  chapters: Chapter[] = [];
  plot: Plot;
  plots: Plot[] = [];

  constructor(private characterService: CharacterService,
              private chapterService: ChapterService,
              private plotService: PlotService,
              private pageChange: Location) {
  }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    console.log('this is from getAll Characters');
    // return this.characterService.getAllCharacters();
    return this.subscription.push(this.characterService.getAllCharacters().subscribe(characters => this.characters = characters));
  }

  getDetail(characters: Character) {
    return this.detail = characters;
  }

  goBack(): void {
    this.pageChange.back();
  }
}
