import {Component, Input, OnInit} from '@angular/core';
import {Character} from '@shared/models/classes/character.model';
import {Location} from '@angular/common';
import {CharacterService} from '@core/services/character/character.service';
import {ChapterService} from '@core/services/chapter/chapter.service';
import {PlotService} from '@core/services/plot/plot.service';
import {Chapter} from '@shared/models/classes/chapter.model';
import {Observable, Subscription} from 'rxjs';
import {State} from '@shared/models/classes/state.model';
import {Plot} from '@shared/models/classes/plot.model';
import {ActivatedRoute} from '@angular/router';
import {environment} from '@env';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.css']
})

export class CharacterDetailComponent implements OnInit {
  @Input() character: Character;

  chapter: Chapter;
  chapters: Chapter[] = [];
  subscription: Subscription[] = [];
  characters: Character[] = [];
  children: Character;
  plot: Plot;
  plots: Plot[] = [];
  id: number = +this.route.snapshot.paramMap.get('id');
  selectedCharacter: Character;
  detail: Character;


  constructor(private characterService: CharacterService,
              private chapterService: ChapterService,
              private plotService: PlotService,
              private route: ActivatedRoute,
              private backlocation: Location) {
  }

  ngOnInit(): void {
    // this.id = +this.route.snapshot.paramMap.get('id');
    this.initCharacter();
  }

  getAll() {
    console.log('this is from character details');
    // return this.characterService.getAllCharacters();
    return this.subscription.push(this.characterService.getAllCharacters().subscribe(characters => this.characters = characters));
  }

  initCharacter() {
    console.log('get by id');
    return this.subscription.push(this.characterService.getCharacterById(this.id).subscribe(character => this.character = character));
  }

  getDetail(characters: Character) {
    return characters.children;
  }

  onSelect(character: Character): void {
    this.selectedCharacter = character;
  }

  goBack(): void {
    this.backlocation.back();
  }

}
