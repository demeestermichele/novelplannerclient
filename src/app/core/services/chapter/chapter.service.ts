import {Injectable, PipeTransform} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {Chapter} from '@shared/models/classes/chapter.model';
import {environment} from '@env';
import {debounceTime, delay, map, switchMap, tap} from 'rxjs/operators';
import {Plot} from '@shared/models/classes/plot.model';
import {Character} from '@shared/models/classes/character.model';
import {SortColumn, SortDirection} from '@shared/table/sortable.directive';
import {DecimalPipe} from '@angular/common';
import {State} from '@shared/models/classes/state.model';


interface SearchResult {
  chapters: Chapter[];
  total: number;
}

// tslint:disable-next-line:max-line-length
const compare = (v1: number | string | Array<Plot> | Array<Character>, v2: number | string | Array<Plot> | Array<Character>) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

function sort(chapters: Chapter[], column: SortColumn, direction: string): Chapter[] {
  if (direction === '' || column === '') {
    return chapters;
  } else {
    return [...chapters].sort((a, b) => {
      const res = compare(a[column], b[column]);
      return direction === 'asc' ? res : -res;
    });
  }
}

// tslint:disable:typedef
function matches(chapter: Chapter, term: string, pipe: PipeTransform) {
  return chapter.name.toLowerCase().includes(term.toLowerCase())
    || pipe.transform(chapter.id).includes(term)
    || pipe.transform(chapter.chapNumber).includes(term)
    || pipe.transform(chapter.characterList).includes(term)
    || pipe.transform(chapter.plotList).includes(term)
    || pipe.transform(chapter.description).includes(term)
    ;
}

/*
const chapterSort: Chapter[] = [];
// tslint:disable:typedef
function matches(text: string, pipe: PipeTransform) {
  return chapterSort.filter(chapter => {
    const term = text.toLowerCase();
    return chapter.name.toLowerCase().includes(term.toLowerCase())
    || pipe.transform(chapter.id).includes(term)
    || pipe.transform(chapter.chapNumber).includes(term)
    || pipe.transform(chapter.characterList).includes(term)
    || pipe.transform(chapter.plotList).includes(term)
    || pipe.transform(chapter.description).includes(term)
    ;
  });
}
*/



@Injectable({
  providedIn: 'root'
})
export class ChapterService {
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
  };

  // tslint:disable:variable-name
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _chapters$ = new BehaviorSubject<Chapter[]>([]);
  private total = new BehaviorSubject<number>(0);
  myChapters: Chapter[] = [];

  private state: State = {
    page: 1,
    pageSize: 4,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };

   constructor(private pipe: DecimalPipe, private http: HttpClient) {
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(200),
      switchMap(() => this._search()),
      delay(200),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._chapters$.next(result.chapters);
      this.total.next(result.total);
    });
    this._search$.next();
  }

  // tslint:disable:typedef
  get chapters$() {
    return this._chapters$.asObservable();
  }

  get total$() {
    return this.total.asObservable();
  }

  get loading$() {
    return this._loading$.asObservable();
  }

  get page() {
    return this.state.page;
  }

  get pageSize() {
    return this.state.pageSize;
  }

  get searchTerm() {
    return this.state.searchTerm;
  }

  // tslint:disable:adjacent-overload-signatures
  set page(page: number) {
    this._set({page});
  }

  set pageSize(pageSize: number) {
    this._set({pageSize});
  }

  set searchTerm(searchTerm: string) {
    this._set({searchTerm});
  }

  set sortColumn(sortColumn: SortColumn) {
    this._set({sortColumn});
  }

  set sortDirection(sortDirection: SortDirection) {
    this._set({sortDirection});
  }


  private _set(patch: Partial<State>) {
    Object.assign(this.state, patch);
    this._search$.next();
  }

  private _search(): Observable<SearchResult> {
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this.state;

    // 1. sort
    let chapters = sort(this.myChapters, sortColumn, sortDirection);

    // 2. filter
    chapters = chapters.filter(chapter => matches(chapter, searchTerm, this.pipe));
    const total = chapters.length;


    // 3. paginate
    chapters = chapters.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({chapters, total});
  }


  getAllChapters(): Observable<Chapter[]> {
    const url = `${environment.chapterUrl}/all`;
    console.log('this is from getAllChapters');
    return this.http.get<Chapter[]>(url, this.httpOptions);
  }

  getChapterById(id: number): Observable<Chapter> {
    const url = `${environment.chapterUrl}/id/${id}`;
    return this.http.get<Chapter>(url, this.httpOptions);
  }

  getChapterByName(name: string): Observable<Chapter> {
    const url = `${environment.chapterUrl}/name/${name}`;
    return this.http.get<Chapter>(url, this.httpOptions);
  }

  getChapterByNumber(num: number): Observable<Chapter[]> {
    const url = `${environment.chapterUrl}/chapNumber/${num}`;
    return this.http.get<Chapter[]>(url, this.httpOptions).pipe(map((chapters: Chapter[]) => chapters.map(chapter => chapter)));
  }

  getChapterByDescription(description: string): Observable<Chapter[]> {
    const url = `${environment.chapterUrl}/description/${description}`;
    return this.http.get<Chapter[]>(url, this.httpOptions).pipe(map((chapters: Chapter[]) => chapters.map(chapter => chapter)));
  }
  getChapterByCharacter(id: number): Observable<Chapter[]>{
    const url = `${environment.chapterUrl}/characters/${id}`;
    return this.http.get<Chapter[]>(url, this.httpOptions).pipe(map((chapters: Chapter[]) => chapters.map(chapter => chapter)));
  }

  postChapter(chapter): Observable<Chapter>{
    return this.http.post<Chapter>(environment.chapterUrl, chapter, this.httpOptions);
  }

  patchChapter(id: number, chapter: Chapter): Observable<Chapter>{
    const url = `${environment.chapterUrl}/id/${id}`;
    return this.http.patch<any>(url, chapter);
  }
}
