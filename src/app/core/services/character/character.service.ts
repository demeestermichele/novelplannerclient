import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Chapter} from '@shared/models/classes/chapter.model';
import {environment} from '@env';
import {Character} from '@shared/models/classes/character.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
  };
  constructor( private http: HttpClient) { }

  getAllCharacters(): Observable<Character[]> {
    const url = `${environment.characterUrl}/all`;
    console.log('this is from getAllCharacters');
    return this.http.get<Character[]>(url, this.httpOptions);
  }

  getCharacterById(id: number): Observable<Character> {
    const url = `${environment.characterUrl}/${id}`;
    return this.http.get<Character>(url, this.httpOptions);
  }
}
