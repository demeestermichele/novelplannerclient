import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {AppRoutingModule} from '../app-routing.module';

@NgModule({
  declarations: [],
  providers: [],
  imports: [
    CommonModule,
    HttpClientModule,
    AppRoutingModule
  ],
  exports: []
})
export class CoreModule {}
