import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {CharacterComponent} from './character/character.component';
import {PlotComponent} from './components/plot/plot.component';
import {ChapterComponent} from './components/chapter/chapter.component';
import {RouterModule} from '@angular/router';
import {DashboardModule} from './dashboard/dashboard.module';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {AppBootstrapModule} from './app-bootstrap/app-bootstrap.module';
import { ChapterListComponent } from './components/chapter/chapter-list/chapter-list.component';
import {SharedModule} from '@shared/shared.module';
import {NgbModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {DecimalPipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbdSortableHeaderDirective} from '@shared/table/sortable.directive';
import {DashboardComponent} from './dashboard/dashboard/dashboard.component';
import { CharacterDetailComponent } from './character/character-detail/character-detail.component';
import { ChapterDetailComponent } from './components/chapter/chapter-detail/chapter-detail.component';
import { PlotListComponent } from './components/plot/plot-list/plot-list.component';
import { PlotDetailComponent } from './components/plot/plot-detail/plot-detail.component';
import { CharacterListComponent } from './character/character-list/character-list.component';

@NgModule({
  declarations: [
    AppComponent,
    CharacterComponent,
    PlotComponent,
    ChapterComponent,
    ChapterListComponent,
    DashboardComponent,
    CharacterDetailComponent,
    ChapterDetailComponent,
    PlotListComponent,
    PlotDetailComponent,
    CharacterListComponent,
    // NgbdSortableHeaderDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserModule,
    RouterModule,
    DashboardModule,
    AppBootstrapModule,
    SharedModule,
    NgbPaginationModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    FormsModule,
      // NgbdSortableHeaderDirective
  ],
  providers: [DecimalPipe, NgbdSortableHeaderDirective],
  bootstrap: [AppComponent]
})
export class AppModule {
}
