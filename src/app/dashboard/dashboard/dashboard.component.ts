import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ChapterService} from '@core/services/chapter/chapter.service';
import {CharacterService} from '@core/services/character/character.service';
import {PlotService} from '@core/services/plot/plot.service';
import {Chapter} from '@shared/models/classes/chapter.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  chapter: Chapter;
  chapters: Chapter[];
  subscription: Subscription[] = [];

  constructor(private http: HttpClient,
              private chapterService: ChapterService,
              characterService: CharacterService,
              plotService: PlotService) {

  }

  ngOnInit(): void {
    this.getAllChapters();
  }

  getAllChapters(){
    this.subscription.push(this.chapterService.getAllChapters().subscribe(chapters => this.chapters = chapters));
  }


  ngOnDestroy(): void {
  }

}
