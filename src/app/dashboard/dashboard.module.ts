import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from '../app-routing.module';
import {HttpClientModule} from '@angular/common/http';



@NgModule({
  declarations: [
    // DashboardComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule
  ],
  bootstrap: [DashboardComponent]
})
export class DashboardModule { }
