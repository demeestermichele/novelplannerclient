import {Component, Injectable, OnDestroy, OnInit, PipeTransform, QueryList, ViewChildren} from '@angular/core';
import {Chapter} from '@shared/models/classes/chapter.model';
import {BehaviorSubject, Observable, of, Subject, Subscription} from 'rxjs';
import {ChapterService} from '@core/services/chapter/chapter.service';
import {DecimalPipe, Location} from '@angular/common';
import {NgbdSortableHeaderDirective, SortColumn, SortDirection, SortEvent} from '@shared/table/sortable.directive';
import {State} from '@shared/models/classes/state.model';
import {Character} from '@shared/models/classes/character.model';
import {CharacterService} from '@core/services/character/character.service';
import {environment} from '@env';
import {map} from 'rxjs/operators';
import {PlotService} from '@core/services/plot/plot.service';

@Component({
  selector: 'app-chapter-list',
  templateUrl: './chapter-list.component.html',
  providers: [ChapterService, DecimalPipe]
})

export class ChapterListComponent implements OnInit, OnDestroy {
  chapters$: Observable<Chapter[]>;
  total$: Observable<number>;
  chapter: Chapter;
  chapters: Chapter[] = [];
  subscription: Subscription[] = [];
  state: State;
  characters: Character[] = [];
  character: Character;
  id: number;
  selectedChapter: Chapter;
  detail: Chapter;

  @ViewChildren(NgbdSortableHeaderDirective) headers: QueryList<NgbdSortableHeaderDirective>;

  // tslint:disable-next-line:max-line-length
  constructor(
    public chapterService: ChapterService,
    public plotService: PlotService,
    private backlocation: Location,
    public sortDir: NgbdSortableHeaderDirective,
    public characterService: CharacterService,
  ) {
    this.chapters$ = chapterService.chapters$;
    this.total$ = chapterService.total$;
  }

  // tslint:disable:typedef
  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.chapterService.sortColumn = column;
    this.chapterService.sortDirection = direction;
  }

  ngOnInit(): void {
    this.getAll();
    this.getChapterCharacters();
  }

  ngOnDestroy(): void {
    this.subscription.forEach(subscription => subscription.unsubscribe());
  }

  goBack(): void {
    this.backlocation.back();
  }

  goTo() {
    this.backlocation.go('/characters');
  }

  getAll() {
    console.log('this is from getAll');
    // return this.chapterService.getAllChapters();
    return this.subscription.push(this.chapterService.getAllChapters().subscribe(chapters => this.chapters = chapters));
  }

  getChapterCharacters() {
    console.log('get characters by chapter');
    return this.subscription.push(this.chapterService.getChapterByCharacter(this.id).subscribe(chapters => this.chapters = chapters));
  }

  onSelect(chapter: Chapter): void {
    this.selectedChapter = chapter;
  }
}
