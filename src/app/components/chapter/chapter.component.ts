import {Component, OnDestroy, OnInit} from '@angular/core';
import {ChapterService} from '@core/services/chapter/chapter.service';
import {Chapter} from '@shared/models/classes/chapter.model';
import {Observable, Subscription} from 'rxjs';
import {Location} from '@angular/common';
import {CharacterService} from '@core/services/character/character.service';
import {PlotService} from '@core/services/plot/plot.service';
import {Character} from '@shared/models/classes/character.model';
import {Plot} from '@shared/models/classes/plot.model';


@Component({
  selector: 'app-chapter',
  templateUrl: './chapter.component.html',
  styleUrls: ['./chapter.component.css']
})
export class ChapterComponent implements  OnInit, OnDestroy{
  subscription: Subscription[] = [];
  characters: Character[] = [];
  chapter: Chapter;
  chapters: Chapter[] = [];
  plot: Plot;
  plots: Plot[] = [];
  detail: Chapter;

  constructor(private chapterService: ChapterService,
              private backlocation: Location,
              private characterService: CharacterService,
              private plotService: PlotService){
this.chapterService = chapterService;
  }

  getAll() {
    console.log('this is from getAll');
    // return this.chapterService.getAllChapters();
    return this.subscription.push(this.chapterService.getAllChapters().subscribe(chapters => this.chapters = chapters));
  }

  getChapter(id: number): any{
    this.subscription.push(this.chapterService.getChapterById(id).subscribe(chapter => this.chapter = chapter));
  }

  getChapterName(name: string): any{
    return this.chapterService.getChapterByName(name);
  }

  ngOnInit(): void {
    this.getAll();
  }

  // onSelect(chapter: Chapter): void {
  //   this.selectedChapter = chapter;
  // }

  ngOnDestroy(): void {
    this.subscription.forEach(subscription => subscription.unsubscribe());
  }
  goBack(): void {
    this.backlocation.back();
  }
}
