import { Component, OnInit, Input } from '@angular/core';
import {Chapter} from '@shared/models/classes/chapter.model';
import {Location} from '@angular/common';
import {CharacterService} from '@core/services/character/character.service';
import {ChapterService} from '@core/services/chapter/chapter.service';
import {PlotService} from '@core/services/plot/plot.service';
import {Subscription} from 'rxjs';
import {Character} from '@shared/models/classes/character.model';
import {Plot} from '@shared/models/classes/plot.model';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-chapter-detail',
  templateUrl: './chapter-detail.component.html',
  styleUrls: ['./chapter-detail.component.css']
})

export class ChapterDetailComponent implements OnInit {
@Input() chapter: Chapter;

  subscription: Subscription[] = [];
  characters: Character[] = [];
  detail: Chapter;
  chapters: Chapter[] = [];
  plot: Plot;
  plots: Plot[] = [];
  selectedChapter: Chapter;
  id: number = +this.route.snapshot.paramMap.get('id');

  constructor(private characterService: CharacterService,
              private chapterService: ChapterService,
              private plotService: PlotService,
              private route: ActivatedRoute,
              private backlocation: Location) {
  }

  ngOnInit(): void {
  this.getAll();
  this.initChapter();
  }

  onSelect(chapter: Chapter): void {
    this.selectedChapter = chapter;
  }

  goBack(): void {
    this.backlocation.back();
  }

  getChapter(id: number): any{
    this.subscription.push(this.chapterService.getChapterById(id).subscribe(chapter => this.chapter = chapter));
  }

  getAll() {
    console.log('this is from getAll');
    // return this.chapterService.getAllChapters();
    return this.subscription.push(this.chapterService.getAllChapters().subscribe(chapters => this.chapters = chapters));
  }

  initChapter(){
    console.log('this is initchapter');
    return this.subscription.push(this.chapterService.getChapterById(this.id).subscribe(chapter => this.chapter = chapter));
  }
}
