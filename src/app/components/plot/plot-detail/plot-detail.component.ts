import {Component, Input, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {CharacterService} from '@core/services/character/character.service';
import {ChapterService} from '@core/services/chapter/chapter.service';
import {PlotService} from '@core/services/plot/plot.service';
import {Subscription} from 'rxjs';
import {Character} from '@shared/models/classes/character.model';
import {Chapter} from '@shared/models/classes/chapter.model';
import {Plot} from '@shared/models/classes/plot.model';

@Component({
  selector: 'app-plot-detail',
  templateUrl: './plot-detail.component.html',
  styleUrls: ['./plot-detail.component.css']
})

export class PlotDetailComponent implements OnInit {
  @Input() plot: Plot;

  character: Character;
  subscription: Subscription[] = [];
  characters: Character[] = [];
  detail: Plot;
  chapter: Chapter;
  chapters: Chapter[] = [];
  plots: Plot[] = [];
  selectedPlot: Plot;

  constructor(private characterService: CharacterService,
              private chapterService: ChapterService,
              private plotService: PlotService,
              private backlocation: Location) {
  }

  ngOnInit(): void {
  }

  onSelect(plot: Plot): void {
    this.selectedPlot = plot;
  }


  goBack(): void {
    this.backlocation.back();
  }
}
