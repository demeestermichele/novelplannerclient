import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import {CharacterService} from '@core/services/character/character.service';
import {ChapterService} from '@core/services/chapter/chapter.service';
import {PlotService} from '@core/services/plot/plot.service';
import {Subscription} from 'rxjs';
import {Character} from '@shared/models/classes/character.model';
import {Chapter} from '@shared/models/classes/chapter.model';
import {Plot} from '@shared/models/classes/plot.model';

@Component({
  selector: 'app-plot',
  templateUrl: './plot.component.html',
  styleUrls: ['./plot.component.css']
})
export class PlotComponent implements OnInit {
  subscription: Subscription[] = [];
  characters: Character[] = [];
  detail: Plot;
  chapter: Chapter;
  chapters: Chapter[] = [];
  plot: Plot;
  plots: Plot[] = [];

  constructor(private characterService: CharacterService,
              private chapterService: ChapterService,
              private plotService: PlotService,
              private backlocation: Location) {
  }

  ngOnInit(): void {
  }

  goBack(): void {
    this.backlocation.back();
  }
}
