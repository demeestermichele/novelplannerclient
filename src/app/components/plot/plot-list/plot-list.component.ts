import { Component, OnInit } from '@angular/core';
import {ChapterService} from '@core/services/chapter/chapter.service';
import {Location} from '@angular/common';
import {CharacterService} from '@core/services/character/character.service';
import {PlotService} from '@core/services/plot/plot.service';
import {Subscription} from 'rxjs';
import {Character} from '@shared/models/classes/character.model';
import {Chapter} from '@shared/models/classes/chapter.model';
import {Plot} from '@shared/models/classes/plot.model';

@Component({
  selector: 'app-plot-list',
  templateUrl: './plot-list.component.html',
  styleUrls: ['./plot-list.component.css']
})
export class PlotListComponent implements OnInit {
  subscription: Subscription[] = [];
  characters: Character[] = [];
  detail: Plot;
  chapter: Chapter;
  chapters: Chapter[] = [];
  plot: Plot;
  plots: Plot[] = [];

  constructor(private characterService: CharacterService,
              private chapterService: ChapterService,
              private plotService: PlotService,
              private backlocation: Location) {
  }

  ngOnInit(): void {
  }
  goBack(): void {
    this.backlocation.back();
  }
}
